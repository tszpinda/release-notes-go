var Notes = {}

/**
 * Templates
 */
Notes.Template = {}

Notes.Template.Row = 
	'<tr>' +
        '<td>{{Key}}</td>' +
        '<td>{{Status}}</td>' +
        '<td>{{Type}}</td>' +
        '<td>{{Date}}</td>' +
        '<td>{{Description}}</td>' +
        '<td>{{ReleaseNotes}}</td>' +
    '</tr>';	
Notes.Template.RowCompiled = Mustache.compile(Notes.Template.Row);
Notes.Template.Error = 
	'<div id="error" class="alert alert-error">' +
  		'<button type="button" class="close" data-dismiss="alert">&times;</button>' +
  		'<strong>Error!</strong> {{text}}' +
	'</div>';
Notes.Template.ErrorCompiled = Mustache.compile(Notes.Template.Error);
/** 
 * To refactor
 */
$(function () {
	if(localStorage.getItem('startRev'))
		$('#startRev').val(localStorage.getItem('startRev'));
	if(localStorage.getItem('endRev'))
		$('#endRev').val(localStorage.getItem('endRev'));

	$('#search').click(function(e){
		e.preventDefault();		
		Notes.UI.showInProgress();
		Notes.UI.hideError();

		setTimeout(function(){
			var startRev = $('#startRev').val();
			var endRev = $('#endRev').val();
			localStorage.setItem('startRev', startRev);
			localStorage.setItem('endRev', endRev);
			Notes.API.load(startRev, endRev)			
	}, 1);		
	});
});
Notes.UI = {}
Notes.UI.showError = function(text) {
	var $html = Notes.Template.ErrorCompiled({"text": text});
	$('.content').append($html);
}
Notes.UI.hideError = function() {
	$('#error').remove();
}
Notes.UI.showInProgress = function() {
	$('#inProgress').show();
	$('#table').hide();
	$('#tableBody').empty();
}
Notes.UI.hideInProgress = function() {
	$('#inProgress').hide();
}
Notes.API = {}
Notes.API.load = function(startRev, endRev) {	
	$.getJSON("/api/notes/" + startRev + "/" + endRev, function(issues){
		var html = "";
		for(var i = 0; i < issues.length; i++)
		{
			var issue = issues[i];
			issue.Date = issue.Date.substring(0, 10);
			html = html +"\n"+ Notes.Template.RowCompiled(issue);
		}
		$('#tableBody').empty().append(html);
		$('#table').show();
		Notes.UI.hideInProgress();
		
	}).error(function(err1, err2, err3) {
		Notes.UI.hideInProgress();
		if(err1 && err1.responseText){
			Notes.UI.showError(err1.responseText);
		}else{
			Notes.UI.showError("Oops there was an error but we don't know the details");
		}
	});
}