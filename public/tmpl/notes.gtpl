<html>
<head>
   <head>
      <title>Release Notes</title>
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
      <link rel="shortcut icon" href="/ui/static/favicon.ico"/>
      
      <link href="/ui/static/bootstrap/css/bootstrap.css" rel="stylesheet">
      <link href="/ui/static/bootstrap/css/bootstrap-responsive.css" rel="stylesheet">    
      <link href="/ui/static/bootstrap/css/main.css" rel="stylesheet" media="screen"/>

      

      <script src="/ui/static/js/jquery.js" type="text/javascript"></script>
      <script src="/ui/static/js/mustache.js" type="text/javascript"></script>
      <!-- jquery-ui used atm only for disabling text selection while using dnd -->
      <script src="/ui/static/js/jquery-ui-1.10.3.custom.js" type="text/javascript"></script>
      <script src="/ui/static/bootstrap/js/bootstrap.js" type="text/javascript"></script>
      
      <script src="/ui/static/js/main.js"></script>
  </script>
   </head>
   <body>      
            <div class="navbar navbar navbar-fixed-top navbar-inverse">
               <div class="navbar-inner">
                  <div class="container">
                     <a class="brand" href="#">Release viewer</a>
                     <form class="navbar-form pull-right">
                        <input type="text" class="span2" placeholder="Start Revision" value="" id="startRev">
                        <input type="text" class="span2" placeholder="End Revision" value="" id="endRev">
                        <button type="submit" class="btn" id="search">Search</button>
                     </form>
                  </div>
               </div>
               <!-- /navbar-inner -->
            </div>
            <div id="inProgress" style="display:none;">Loading...</div>
            <div class="content">
               
               <table class="table table-striped table-condensed" style="display:none;" id="table">
                  <thead>
                     <tr>
                        <th class="header" style="width: 65px;">Issue</th>
                        <th class="header" style="width: 135px;">Status</th>
                        <th class="header" style="width: 110px;">Type</th>
                        <th class="header" style="width: 70px;">Date</th>
                        <th class="header">Description</th>
                        <th class="header">Release Notes</th>
                     </tr>
                  </thead>
                  <tbody  id="tableBody">
                  </tbody>
               </table>
            </div>
   </body>
</html>
