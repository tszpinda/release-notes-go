package main

import (
	"github.com/tszpinda/gorest"
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	vcs "github.com/tszpinda/go-vcs"
	"html/template"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"sort"
	"strings"
	"time"
)

type Issue struct {
	Key          string
	Status       string
	Type         string
	Date         time.Time
	Description  string
	ReleaseNotes string
}

func main() {
	schedule(loadLogs, 240*time.Second)

	initApi()
	initUI()
	var port = os.Getenv("PORT")
	if port == "" {
		port = "8080"
	}
	fmt.Println("starting app on port: " + port)
	log.Fatal(http.ListenAndServe(":"+port, nil))
}

func initUI() {
	fileHandler := http.StripPrefix("/ui/static/", http.FileServer(http.Dir("./public/static")))
	http.Handle("/ui/static/", fileHandler)
	http.HandleFunc("/", homePage)
}

func homePage(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {
		var templateName = "public/tmpl/notes.gtpl"
		t, err := template.ParseFiles(templateName)
		if err != nil {
			log.Fatal("unable to parse ", templateName, err)
		}
		t.Execute(w, nil)
	}
}

func initApi() {
	gorest.RegisterService(new(ReleaseNotesService))
	http.Handle("/api/", gorest.Handle())
}

type ReleaseNotesService struct {
	gorest.RestService `root:"/api/" consumes:"application/json" produces:"application/json"`
	notes              gorest.EndPoint `method:"GET" path:"/notes/{startRev:string}/{endRev:string}"`
	notesNoEndRev      gorest.EndPoint `method:"GET" path:"/notes/{startRev:string}"`
	notesNoRev         gorest.EndPoint `method:"GET" path:"/notes/"`
}

func (serv ReleaseNotesService) NotesNoEndRev(startRev string) []Issue {
	logs, err := logs(startRev, "")
	if err != nil {
		serv.ResponseBuilder().SetResponseCode(404).WriteAndOveride([]byte(err.Error()))
		return nil
	}
	return serv.toIssueList(logs)
}
func (serv ReleaseNotesService) NotesNoRev() []Issue {
	logs, err := logs("", "")
	if err != nil {
		serv.ResponseBuilder().SetResponseCode(404).WriteAndOveride([]byte(err.Error()))
		return nil
	}
	return serv.toIssueList(logs)
}
func (serv ReleaseNotesService) Notes(startRev, endRev string) []Issue {
	logs, err := logs(startRev, endRev)
	if err != nil {
		serv.ResponseBuilder().SetResponseCode(404).WriteAndOveride([]byte(err.Error()))
		return nil
	}
	return serv.toIssueList(logs)
}

func (serv ReleaseNotesService) toIssueList(logs []vcs.Log) []Issue {
	issues := convertLogs(logs)
	logs = nil
	populateFromJira(issues)

	issueList := make([]Issue, 0)

	for _, issue := range issues {
		if issue.Description != "" {
			issueList = append(issueList, issue)
		}
	}

	return sortLogs(issueList)
}

type Status struct {
	Name string
}
type IssueType struct {
	Name string
}
type Fields struct {
	Summary      string
	IssueType    IssueType
	Status       Status
	ReleaseNotes string `json:"customfield_11750"`
}
type JiraIssue struct {
	Fields Fields
	Key    string
}
type JiraResponse struct {
	Issues        []JiraIssue
	ErrorMessages []string
}

func populateFromJira(issues map[string]Issue) map[string]Issue {
	jiraUrl := "https://<change>.jira.com/rest/api/latest/search?validateQuery=false"
	jiraUsername := "auser"
	jiraPassword := "auser"

	if len(issues) == 0 {
		return issues
	}

	client := &http.Client{}
	keys := ""
	for _, issue := range issues {
		if !strings.Contains(issue.Key, ",") {			
			keys = keys + issue.Key + ","
		}else{
			fmt.Printf("Invalid key: %v\n", issue);
		}
	}

	keys = keys[0 : len(keys)-1]

	query := `{"jql":"key in (` + keys + `)", "startAt":0, "maxResults": 1000, "validateQuery": false}`
	fmt.Printf("Request body: %v\n", query)
	req, err := http.NewRequest("POST", jiraUrl, strings.NewReader(query))
	if err != nil {
		panic(err)
	}

	auth := base64.StdEncoding.EncodeToString([]byte(jiraUsername + ":" + jiraPassword))
	auth = "Basic " + auth
	req.Header.Set("Authorization", auth)
	req.Header.Set("Content-Type", "application/json")
	resp, err := client.Do(req)
	if err != nil {
		panic(err)
	}

	rawResponse, err := ioutil.ReadAll(resp.Body)
	resp.Body.Close()
	if err != nil {
		panic(err)
	}

	jiraResponse := JiraResponse{}
	if err := json.Unmarshal(rawResponse, &jiraResponse); err == nil {
		if len(jiraResponse.ErrorMessages) > 0 {
			log.Println("JIRA Errors:")
			for _, err := range jiraResponse.ErrorMessages {
				log.Printf("%v\n", err)
			}
		} else {
			log.Println("JIRA Response: ok")
		}
	} else {
		panic(err)
	}
	log.Printf("Jira returned: %v", len(jiraResponse.Issues))
	for _, jiraIssue := range jiraResponse.Issues {
		issue := issues[jiraIssue.Key]
		issue.Description = jiraIssue.Fields.Summary
		issue.ReleaseNotes = jiraIssue.Fields.ReleaseNotes
		issue.Status = jiraIssue.Fields.Status.Name
		issue.Type = jiraIssue.Fields.IssueType.Name
		issues[issue.Key] = issue
	}
	return issues
}

func convertLogs(logs []vcs.Log) map[string]Issue {
	issues := make(map[string]Issue)
	for _, l := range logs {
		key := getKey(l.Message)
		if key == "" {
			continue
		}
		_, exists := issues[key]
		if exists {
			continue
		}

		issue := Issue{}
		issue.Key = key
		issue.Date = l.Date
		issues[key] = issue
	}
	return issues
}

func getKey(msg string) (key string) {

	if strings.Index(msg, "OSD") == 0 {
		arr := strings.Split(msg, " ")
		if len(arr) > 0 {
			key = arr[0]
			if len(key) < 5 && len(key) > 9 {
				key = ""
			}
		}
	}

	return key
}

func gitLogs() ([]vcs.Log, error) {
	checkoutdir := os.Getenv("CHECKOUT_DIR")
	if checkoutdir == "" {
		tmpdir, err := ioutil.TempDir("", "release-notes-checkout")
		if err != nil {
			log.Printf("unable to create tmp directory: \n%s", err)
			return nil, fmt.Errorf("Unable to create tmp dir")
		}
		checkoutdir = tmpdir
	}
	//checkoutdir = "/Users/tszpinda/tmp/release-notes"

	gitUrl := "https://<username>:<password>@bitbucket.org/tszpinda/<repo>"
	log.Println("Checkout dir: ", checkoutdir)
	r, err := vcs.CloneOrOpen(vcs.Git, gitUrl, checkoutdir)
	if err != nil {
		log.Printf("clone or open failed: \n%s", err)
		return nil, fmt.Errorf("Unable to clone repo")
	}
	_, err = r.CheckOut("master")
	if err != nil {
		log.Printf("git master checkout failed: \n%s", err)
		return nil, fmt.Errorf("Unable to checkout master")
	}
	err = r.Pull()
	if err != nil {
		log.Printf("git pull failed: \n%s", err)
		return nil, fmt.Errorf("Unable to Pull master")
	}
	log.Println("Retriving logs")
	logs, err := r.Log("", "")
	if err != nil {
		log.Printf("git log failed: \n%s", err)
		return nil, fmt.Errorf("One or both revisions not found")
	}

	return logs, nil
}

var logCache []vcs.Log

func schedule(what func(), delay time.Duration) chan bool {
	stop := make(chan bool)

	go func() {
		for {
			what()
			select {
			case <-time.After(delay):
			case <-stop:
				return
			}
		}
	}()

	return stop
}

var logLoading = false

func stoppedLoading() {
	logLoading = false
}
func loadLogs() {
	if !logLoading {
		logLoading = true
		defer stoppedLoading()
		cache, e := gitLogs()
		logCache = cache
		if e != nil {
			fmt.Println("Error loading logs", e)
		}
	}
}

func trunkateLogRev(logRev string) string {
	if len(logRev) <= 7 {
		return logRev
	}
	
	return logRev[:7]
}

func logs(startRev, endRev string) ([]vcs.Log, error) {
	startRev = trunkateLogRev(startRev)
	endRev = trunkateLogRev(endRev)
	
	if len(logCache) == 0 {
		return nil, errors.New("Please wait, working...")
	}
	log.Println("Logs loaded: ", len(logCache))
	filtered := make([]vcs.Log, 0)
	endSet := false
	for _, l := range logCache {
		if endRev == l.Hash {
			log.Println("endSet=true")
			endSet = true
		}
		if endSet {
			if startRev == l.Hash {
				log.Println("logs: done:1")
				return filtered, nil
			}
			filtered = append(filtered, l)
		}
	}
	log.Println("logs: done:2")
	return filtered, nil
}

type Issues []Issue

func (l Issues) Len() int      { return len(l) }
func (l Issues) Swap(i, j int) { l[i], l[j] = l[j], l[i] }

type ByDate struct{ Issues }

func (l ByDate) Less(i, j int) bool {
	l1 := l.Issues[i]
	l2 := l.Issues[j]
	return l1.Date.Before(l2.Date)
}

func sortLogs(issues []Issue) []Issue {
	for _, i := range issues {
		fmt.Print("[", i.Key, " ", i.Date, "],")
	}
	sort.Sort(ByDate{issues})
	fmt.Println("after:")
	for _, i := range issues {
		fmt.Print("[", i.Key, " ", i.Date, "],")
	}
	return issues
}
