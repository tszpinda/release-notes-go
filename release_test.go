package main

import (
	"fmt"
	//	"io/ioutil"
	//	"os"
	vcs "github.com/tszpinda/go-vcs"
	"testing"
	"time"
	"encoding/base64"
)

func TestAuthToken(t *testing.T) {
	jiraUsername := "aUser"
	jiraPassword := "aPassword"

	auth := base64.StdEncoding.EncodeToString([]byte(jiraUsername + ":" + jiraPassword))
	auth = "Basic " + auth
	fmt.Println(auth)
}

func TestConvertLogs(t *testing.T) {
	t.Parallel()

	log1 := vcs.Log{"1didsa", "Jim Bill", time.Now(), "OSD-1234 - Been there done that"}
	log2 := vcs.Log{"1didsx", "Jim Bill", time.Now(), "OSD-123 - Been there done that"}
	log3 := vcs.Log{"1didsb", "Jim Bill", time.Now(), "OSD-123 - Been there done that"}
	log4 := vcs.Log{"1didsb", "Jim Bill", time.Now(), "Merge OSD-123 - Been there done that"}

	logs := []vcs.Log{log1, log2, log3, log4}

	issues := convertLogs(logs)
	if len(issues) != 2 {
		t.Fatalf("Expected 2 but was: %v", len(issues))
	}

}
func TestTrunkateLogRev(t *testing.T) {

	key := trunkateLogRev("c9fea6be72b1c42135b6e44357d9803f4dc68678")
	if key != "c9fea6b" {
		t.Fatalf("invalid key was: %v", key)
	}
	key = trunkateLogRev("c9fea6b")
	if key != "c9fea6b" {
		t.Fatalf("invalid key was: %v", key)
	}
	key = trunkateLogRev("c9fea6b1")
	if key != "c9fea6b" {
		t.Fatalf("invalid key was: %v", key)
	}
	key = trunkateLogRev("c9fea6")
	if key != "c9fea6" {
		t.Fatalf("invalid key was: %v", key)
	}
	key = trunkateLogRev("c9f")
	if key != "c9f" {
		t.Fatalf("invalid key was: %v", key)
	}
	key = trunkateLogRev("")
	if key != "" {
		t.Fatalf("invalid key was: %v", key)
	}
}

func TestGetKey(t *testing.T) {
	key := getKey("OSD-1234 - Axjc")
	if key != "OSD-1234" {
		t.Fatalf("invalid key was: %v", key)
	}

	key = getKey("OSD-123 - Axjc")
	if key != "OSD-123" {
		t.Fatalf("invalid key was: %v", key)
	}

	key = getKey("Merge OSD-123 - Axjc")
	if key != "" {
		t.Fatalf("invalid key was: %v", key)
	}
}

func TestPopulateFromJira(t *testing.T) {
	issueKeys := []string{"OSD-1803", "OSD-1804", "OSD-1937"}
	issues := make(map[string]Issue)

	for _, key := range issueKeys {
		issue := Issue{}
		issue.Key = key
		issue.Date = time.Now()
		issues[issue.Key] = issue
	}

	jiraIssue := populateFromJira(issues)["OSD-1803"]

	if jiraIssue.Description == "" {
		t.Fatal("description was empty")
	}

	if jiraIssue.ReleaseNotes == "" {
		t.Fatal("release notes were empty")
	}

	if jiraIssue.Status == "" {
		t.Fatal("status was empty")
	}

	if jiraIssue.Type == "" {
		t.Fatal("type was empty")
	}
}

func TestSortLogs(t *testing.T) {
	t1, _ := time.Parse("2006-01-02 15:04", "2011-01-11 22:15")
	t2, _ := time.Parse("2006-01-02 15:04", "2011-01-12 12:13")
	t3, _ := time.Parse("2006-01-02 15:04", "2011-01-13 9:15")
	t4, _ := time.Parse("2006-01-02 15:04", "2011-01-14 12:15")

	log1 := Issue{"OSD-1", "a", "b", t1, "OSD-1", ""}
	log2 := Issue{"OSD-2", "a", "b", t2, "OSD-2", ""}
	log3 := Issue{"OSD-3", "a", "b", t3, "OSD-3", ""}
	log4 := Issue{"OSD-4", "a", "b", t4, "OSD-4", ""}

	logs := sortLogs([]Issue{log3, log1, log4, log2})

	if logs[0].Key != "OSD-1" {
		t.Fatal("Expected OSD-1 but was: ", logs[0].Key)
	}

	if logs[1].Key != "OSD-2" {
		t.Fatal("Expected OSD-2 but was: ", logs[1].Key)
	}

	if logs[2].Key != "OSD-3" {
		t.Fatal("Expected OSD-3 but was: ", logs[2].Key)
	}

	if logs[3].Key != "OSD-4" {
		t.Fatal("Expected OSD-4 but was: ", logs[3].Key)
	}
}
